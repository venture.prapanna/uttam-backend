const fs= require("fs")

deleteFile = (path, filename) => {
    
    if(filename && fs.existsSync(path+filename)){
        
        fs.unlinkSync(path+filename)
    }

}

module.exports = {deleteFile}