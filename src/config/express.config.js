const express = require("express");
const app = express();
require("./mongodb.config");
const routes = require("../routes");

const { ZodError } = require("zod");
const path = require("path");

const { JsonWebTokenError, TokenExpiredError } = require("jsonwebtoken");
const cookieParser = require("cookie-parser");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(cookieParser())

app.use("/api", routes);

app.use((req, res, next) => {
  next({ code: 404, message: "Not found" });
});

app.use((error, req, res, next) => {
  console.log(error);

  code = error.code ?? 500;
  let msg = error.message ?? "internal server error";
  let result = error.result ?? null;
  if (error instanceof ZodError) {
    let errorMsg = {};
    error.errors.map((errorObj) => {
      errorMsg[errorObj.path[0]] = errorObj.message;
    });
    code = 400;
    msg = errorMsg;
    message = "Validation failure";
  }
  // if(error instanceof JsonWebTokenError || error instanceof TokenExpiredError){
  //     code = 401,
  //     msg = error.message;
  // }
  // res.status(code).json({
  //     result:null,
  //     msg:msg,
  //     meta:null

  // })
  if (error.code === 11000) {
    code = 400;

    let uniqueKeys = Object.keys(error.keyPattern);
    let msgBody = uniqueKeys.map((key) => {
      return {
        [key]: key + " should be unique",
      };
    });
    result = msgBody;
    msg = "Validation Fail";
  }

  res.status(code).json({
    result: result,
    msg: msg,
    meta: null,
  });
});

module.exports = app;
