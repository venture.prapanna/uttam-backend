const dotenv = require("dotenv")
dotenv.config()

const mongoose = require("mongoose");

mongoose.connect(process.env.MONGODB_URL,{
    dbName:process.env.MONGODB_NAME,
    autoIndex:true,
    autoCreate:true
})

.then((db)=>{
    console.log("DataBase Connected Successfully")

})

.catch((error)=>{
    console.log("Error connecting Database server")
    throw error;
})