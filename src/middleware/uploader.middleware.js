const multer = require("multer");
const fs = require("fs");

const mystorage = multer.diskStorage({
  destination: (req, file, cb) => {
    let path = req.uploadDir ?? "./public/uploads";
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path,{recursive:true});
    }
    cb(null, path);
  },

  filename: (req, file, cb) => {
    const ext = file.originalname.split(".").pop();
    const name = Date.now() + "." + ext;

    cb(null, name);
  },
});

const imageFilter = (req, file, cb) => {
  const allowed = ["jpeg", "png", "jpg", "svg", "mkv"];
  const ext = file.originalname.split(".").pop();

  if (allowed.includes(ext.toLowerCase())) {
    cb(null, true);
  } else {
    cb({ code: 400, msg: "File type not supported" }, null);
  }
};

const uploader = multer({
  storage: mystorage,
  fileFilter: imageFilter,
  limits: {
    fileSize: 3000000,
  },
});



module.exports = uploader;
