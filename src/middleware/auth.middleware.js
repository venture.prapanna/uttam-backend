const jwt = require('jsonwebtoken');
const UserModel = require('../app/auth/user.model');

const CheckLogin = async (req, res, next) => {
    try {
    
        // const token = req.cookies.access_token;
        
        // if (!token) {
        //     return next({ code: 401, message: "Login required." });
        // }

      
        // const decoded = jwt.verify(token, process.env.JWT_SECRET);
        // console.log(decoded)

       
        // const userDetail = await UserModel.findOne({ _id: decoded.id });
        // if (!userDetail) {
        //     return next({ code: 401, message: "User does not exist." });
        // }

     
        // req.authUser = userDetail;

     
        next();
    } catch (exception) {
        console.error(exception);
        next({ code: 401, message: "Invalid or expired token." });
    }
};

module.exports = CheckLogin;
