const TestimonialModel = require("./testimonial.model")

class TestimonialService{
    updateById = async (bannerId, payload) => {
        try {
            let response = await TestimonialModel.findByIdAndUpdate(bannerId, {
                $set: payload
            },{new:true})
            return response
        } catch(exception) {
            throw exception
        }
    }

    deleteById = async(testimonialId) => {
        try {
            let response = await TestimonialModel.findByIdAndDelete(testimonialId)
            if(response) {
                return response
            } else {
                throw {code: 404, message: " Testimonial data already deleted or does not exists"}
            }
        } catch(exception) {
            throw exception;
        }
    }
}

const testimonialSvc= new TestimonialService()
module.exports = testimonialSvc;