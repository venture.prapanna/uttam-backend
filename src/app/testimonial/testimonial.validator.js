const {z} = require("zod")
const TestimonialCreateSchema = z.object({
    name:z.string().min(1).max(100),
    company:z.string().min(1).max(500),
    comment:z.string()
    

})

const TestimonialUpdateSchema = z.object({
   name:z.string().min(1).max(100),
    company:z.string().min(1).max(500),
    comment:z.string()
    
})
module.exports = {TestimonialCreateSchema,TestimonialUpdateSchema}


