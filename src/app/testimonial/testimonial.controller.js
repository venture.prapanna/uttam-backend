
const { deleteFile } = require("../../config/helpers");
const TestimonialModel=require("./testimonial.model");
const testimonialSvc = require("./testimonial.service");

class TestimonialController {
    createTestimonial=async(req,res,next)=>{
        try{
            const data= req.body;
            const image= req.file?req.file.filename:null;
            data.photo= image
            const existingTestimonial = await TestimonialModel.findOne({ photo: image });
            console.log(existingTestimonial)
            if (existingTestimonial) {
                return res.status(400).json({ error: 'Photo must be unique' });
            }
    
            const testimonial= new TestimonialModel(data)
            const testimonialDetail= await testimonial.save()
            res.json({
                result:testimonialDetail,
                message:"Testimonial Created Successfully"
            })

        }catch(exception){
            next(exception)
        }
    }
    getTestimonialsById = async (req, res, next) => {
        try{
            console.log("mers")
            const testimonials= await TestimonialModel.findById(req.params.id)
            if(!testimonials){
                console.log("here")
                throw {code: 404, message: "Testimonial does not exist"};
               
            }
            res.json({
                result:testimonials,
                message:"Testimonials Fetched successfully"
            })

        }catch(exception){
            next(exception)
        }
    }

    getAllTestimonials=async(req,res,next)=>{
        try{
            const testimonials= await TestimonialModel.find()
            if(testimonials.length===0){
                throw {code: 404, message: "Testimonial does not exist"};
                console.log("here")
            }
            res.json({
                result:testimonials,
                message:"Testimonials Fetched successfully"
            })

        }catch(exception){
            next(exception)
        }
    }

    updateTestimonial=async(req,res,next)=>{
        try{
            console.log(req.params.id)
            const testimonial= await TestimonialModel.findOne({_id:req.params.id})
            if (!testimonial) {
                throw {code: 404, message: "Testimonial does not exist"};
            }
        

            const data= req.body;
            console.log("data is",data)
            const image = req.file?req.file.filename:null;
            console.log("image is",image)
            data.photo= image;
            console.log("data is",data.photo)
           
            console.log("testimonial is",testimonial)
             let oldImage= image;
             console.log("old image is",oldImage)
             if(!oldImage || oldImage===""){
                oldImage=testimonial.image;
             }
            
             const oldTestimonial= await testimonialSvc.updateById(req.params.id,{...data,photo:oldImage})
             console.log("oldTestimonial is",oldTestimonial)
             console.log("oldTestimonial image is",oldTestimonial.photo)
             
             if(image){
                deleteFile("./public/uploads/testimonial/",oldTestimonial.photo)
             }
             res.json({
                result:oldTestimonial,
                message:"Testimonial Updated Successfully",
                meta:null,
             })
        
            }catch(exception){
            next(exception)
        }
    }

    deleteTestimonial=async(req,res,next)=>{
        try{
          let  testimonialId=req.params.id;
            console.log(req.params.id)
            const testimonial= await TestimonialModel.findOne({_id:req.params.id})
            if (!testimonial) {
                throw {code: 404, message: "Testimonial does not exist"};
            }
        

            console.log("testimonial is",testimonial)
            let deleteTestimonial = await testimonialSvc.deleteById(testimonialId)
            if(deleteTestimonial.image) {
                deleteFile('./public/uploads/testimonial/', deleteTestimonial.photo)
            }
            res.json({
                result: deleteTestimonial, 
                message: "Testimonial Deleted successfully",
                meta: null
            })
           
        
            }catch(exception){
            next(exception)
        }
    }

}
const TestimonialCtrl= new TestimonialController()
module.exports=TestimonialCtrl