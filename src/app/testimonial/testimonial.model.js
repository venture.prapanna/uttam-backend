const mongoose = require("mongoose");

const testimonialSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  company: {
    type: String,
    required: true,
  },
  comment: {
    type: String,
    required: true,
  },
  photo: {
    type: String,
    required: true,
    unique: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const TestimonialModel = mongoose.model("Testimonial", testimonialSchema);

module.exports = TestimonialModel;
