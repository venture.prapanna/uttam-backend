

const express = require('express');
const router = express.Router();
const TestimonialCtrl= require('./testimonial.controller');

const uploader = require('../../middleware/uploader.middleware');
const { ValidateRequest } = require('../../middleware/validator.middleware');
const { TestimonialCreateSchema, TestimonialUpdateSchema } = require('./testimonial.validator');
const CheckLogin = require('../../middleware/auth.middleware');
const CheckPermission = require('../../middleware/rbac.middleware');

const dirSet = (req, res, next) => {
    req.uploadDir = "./public/uploads/testimonial/"
    next()
}



router.post('/',
CheckLogin,
dirSet,CheckPermission('admin'),uploader.single("photo"), ValidateRequest(TestimonialCreateSchema),TestimonialCtrl.createTestimonial);

router.get('/',
CheckLogin,CheckPermission("admin"),
 TestimonialCtrl.getAllTestimonials);
router.get('/:id',
CheckLogin,CheckPermission("admin"), 
TestimonialCtrl.getTestimonialsById);




router.put('/:id',CheckLogin,dirSet,CheckPermission('admin'),uploader.single("photo"),ValidateRequest(TestimonialUpdateSchema) ,TestimonialCtrl.updateTestimonial);


router.delete('/:id',CheckLogin,CheckPermission('admin'), TestimonialCtrl.deleteTestimonial);

module.exports = router;
