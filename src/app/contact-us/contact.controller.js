const ContactModel = require("./contact.model")
class ContactController {
createContact = async(req,res,next)=>{
    try{
        let  data = req.body;
        const contact = new ContactModel(data)
        const contactDetail = await contact.save()
        res.json({
            result:contactDetail,
            message:"Contact form created"
        })


    }catch(exception){
        throw exception
    }


}
getContactById = async(req,res,next)=>{
    try{
        let  id = req.params.id;
        const detail = await ContactModel.findById(id)
        if(!detail){
          
            throw {code: 404, message: "Contact does not exist"};
           
        }
        res.json({
            result:detail,
            message:`Contact us  detail of id ${id} fetched successfully`

        })


    }catch(exception){
        throw exception;
    }
}

getAllContacts = async(req,res,next)=>{
    try{
        const allContact = await ContactModel.find();
        if(allContact.length===0){
          
            throw {code: 404, message: "Contact does not exist"};
           
        }
        res.json({
            result:allContact,
            message:"All contact detailed fetched  Successfully"

        })

    }catch(exception){
        throw exception;
    }
}


}
const contactCtrl = new ContactController()
module.exports = contactCtrl;