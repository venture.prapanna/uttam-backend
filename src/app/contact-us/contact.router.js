const express = require("express")
const router = express.Router()
const contactCtrl= require("./contact.controller.js")

router.post("/",contactCtrl.createContact)
router.get("/:id",contactCtrl.getContactById);
router.get("/",contactCtrl.getAllContacts);

module.exports= router;