const mongoose = require("mongoose");

const ContactSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    required: true,
    unique: true,
  },
   interested:{
    type:String,
    required:true

   },

  message:{
    type:String,
    required:true,

  },
  phone:String,

},
  {
    timestamps:true,
    autoIndex:true,
    autoCreate:true
  }
);

const ContactModel = mongoose.model("Contact", ContactSchema);
module.exports = ContactModel;
