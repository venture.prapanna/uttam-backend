const express = require('express');
const router = express.Router();
const uploader= require("../../middleware/uploader.middleware")
const teamCtrl = require('./teams.controller.js');
const { TeamsCreateSchema } = require('./teams.validator.js');
const {ValidateRequest} = require('../../middleware/validator.middleware.js');
const CheckLogin = require('../../middleware/auth.middleware.js');
const CheckPermission =require("../../middleware/rbac.middleware.js")

const dirSet = (req, res, next) => {
    req.uploadDir = "./public/uploads/teampic/"
    next()
}

router.post('/', CheckLogin,dirSet,CheckPermission('admin'),uploader.single("photo"),ValidateRequest(TeamsCreateSchema),teamCtrl.createTeamMember);


router.get('/',CheckLogin,CheckPermission('admin'), teamCtrl.getAllTeamMembers);


router.get('/:id',CheckLogin,CheckPermission('admin'), teamCtrl.getTeamMemberById);
router.delete('/:id',CheckLogin,CheckPermission('admin'), teamCtrl.deleteTeamMember);

module.exports = router;
