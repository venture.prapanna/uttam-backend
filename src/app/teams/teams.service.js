const TeamMember = require("./teams.model");
const { TeamsCreateSchema } = require("./teams.validator");


class TeamsService {
    transformTeamMemberCreateSchema(request) {
        let teamMember = {
          ...request.body,
        };
    
        if (!request.file) {
          throw {
            code: 400,
            message: "Validation Failure",
            result:teamMember};
        } else if (request.file) {
          teamMember.photo= request.file.filename;
        }
        console.log("t",teamMember)
        return teamMember;
        
      }
  createTeams= async (data) => {
    try {
      console.log(data)
      const validatedData = TeamsCreateSchema.parse(data);
      console.log(validatedData)
      let Teams= new TeamMember(validatedData);
      return await Teams.save();
    } catch (exception) {
      throw exception;
    }
  };

  
}
const teamsSvc = new TeamsService();
module.exports = teamsSvc;
