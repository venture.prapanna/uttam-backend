

const {z} = require("zod")
const TeamsCreateSchema = z.object({


   

      name: z.string().min(1), 
      position: z.string().min(1),
      email:z.string(),
      photo:z.string(),
      linkedinUrl: z.string().optional(), 
      twitterUrl: z.string().optional(), 

    

    
})

const TeamsUpdateSchema = z.object({
    name:z.string().min(1).max(100),
    position:z.string().min(1).max(500),
    
    

})
module.exports = {TeamsCreateSchema,TeamsUpdateSchema}


