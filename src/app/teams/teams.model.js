const mongoose = require("mongoose");

const teamMemberSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    unique: true,
  },
  position: {
    type: String,
    required: true,
  },
  photo: {
    type: String,
    required: true,
    unique: true,
  },
  linkedinUrl: String,
  twitterUrl: String,
});

const TeamMember = mongoose.model("TeamMember", teamMemberSchema);

module.exports = TeamMember;
