const TeamMember = require("./teams.model.js");
const teamsSvc = require("./teams.service.js");
class TeamController {
  // createTeamMember = async (req, res,next) => {
  //   try {
  //     console.log('there')
  //     console.log(req.body)
  //     let photo = req.file.filename;
  //     console.log(photo)
  //     const { name, position,linkedinUrl, twitterUrl } = req.body;
  //     // if (!photo) {
  //     //     return res.status(400).json({ message: 'Photo field is required' });
  //     //   }

  //     const newTeamMember = new TeamMember({
  //       name,
  //       position,
  //       photo,
  //       linkedinUrl,
  //       twitterUrl
  //     });
  //     const savedTeamMember = await newTeamMember.save();
  //     console.log(saved)
  //     res.json({

  //         result:savedTeamMember,
  //         message:"Team member Added"

  //     });
  //   } catch (exception) {
  //      next(exception)
  //   }
  // };

  // createTeamMember = async (req, res,next) => {
  //     try {
  //       console.log(req.body)
  //       console.log(req.file)
  //       let data = teamsSvc.transformTeamMemberCreateSchema(req)
  //       console.log(data)
  //       const validated= await teamsSvc.createTeams(data)

  //       console.log(validated)

  //       res.json({

  //           result:validated,
  //           message:"Team member Added"

  //       });
  //     } catch (exception) {
  //        next(exception)
  //     }
  //   };

  createTeamMember = async (req, res, next) => {
    try {
      console.log(req.body);
      console.log(req.file);
      let data = teamsSvc.transformTeamMemberCreateSchema(req);
      console.log(data);
      const validated = await teamsSvc.createTeams(data);

      console.log(validated);

      res.json({
        result: validated,
        message: "Team member Added",
      });
    } catch (exception) {
      next(exception);
    }

   
  };

  getTeamMemberById = async (req, res, next) => {
    try {
       console.log("member")
      console.log("herefdfd");
      let id = req.params.id;
      const MemberById = await TeamMember.findById(id);
      console.log(MemberById)
      res.json({
        result: MemberById,
        message: "Team Member having id {`{id}`} fetched successfully",
      });
    } catch (exception) {
      next(exception);
    }
  };

  getAllTeamMembers = async (req, res, next) => {
    try {
      const teamMembers = await TeamMember.find();

      res.json({
        result: teamMembers,
        message: "Team members fetched successfully",
      });
    } catch (exception) {
      next(exception);
    }
  };

  deleteTeamMember = async (req, res, next) => {
    try {
      await TeamMember.findByIdAndDelete(req.params.id);
      res.json({
        message: "Team member deleted",
      });
    } catch (exception) {
      next(exception);
    }
  };
}
const teamCtrl = new TeamController();
module.exports = teamCtrl;
