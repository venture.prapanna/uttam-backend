const express = require("express")
const router = express.Router()
const  AuthCtrl = require("./auth.controller.js")
const uploader = require("../../middleware/uploader.middleware");
const {ValidateRequest,LoginRequest} = require("../../middleware/validator.middleware");

const { registerSchema, passwordSchema, loginSchema, emailValidatonSchema } = require("./auth.validator");

const dirSetup = (req, res, next) => {
    req.uploadDir = "./public/uploads/users";
    next()
}

router.post("/register",dirSetup,uploader.single("photo"),ValidateRequest(registerSchema), AuthCtrl.registerUser)
router.post("/login",LoginRequest(loginSchema), AuthCtrl.loginUser)

module.exports = router