const UserModel = require("./user.model.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

class AuthController {
  registerUser = async (req, res, next) => {
  try {
    let userData = req.body;
  
     const imageName= req.file? req.file.filename :null;
    
    const hashedPassword = bcrypt.hashSync(userData.password, 10);
    userData.photo=imageName;
    userData.password=hashedPassword;
   
    const newUser = new UserModel(
      userData
    );
  
    const user = await newUser.save();
    
    const{password,...rest}= user._doc;
    res.json({

      result: rest,
      message: "User Created Successfully",
    });
  } catch (exception) {
    next(exception);
  }
};

  loginUser = async (req, res, next) => {
    try {
      const { email, password } = req.body;

      let UserDetail = await UserModel.findOne({
        email,
      });

      if (!UserDetail) {
        return next({ code: 400, message: "User does not exists" });
      }
      const validPassword = bcrypt.compareSync(password, UserDetail.password);

      if (!validPassword) {
        return next({
          code: 400,
          message: "Invalid Credentials",
        });
      }
      let token = jwt.sign(
        {
          id: UserDetail._id,
        },
        process.env.JWT_SECRET,
        {
          expiresIn: "1h",
        }
      );
      const { password: passkey, ...rest } = UserDetail._doc;
      res
        .cookie("access_token", token, {
          httpOnly: true,
        })

        .json({
          result: {rest,token},

          mesage: "User Logged in Successfully",
        });
    } catch (exception) {
      next(exception);
    }
  };
}

const AuthCtrl = new AuthController();
module.exports = AuthCtrl;
