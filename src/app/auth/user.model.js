const  mongoose =require("mongoose")

const UserSchema = mongoose.Schema({

   name:{
    type:String,
    required:true,
    unique:true,
   },
   email:{
      type:String,
      required:true,
      unique:true,
   },
   
   password:{
    type:String,
    required:true,

   },
    photo:String,
    linkedinUrl: String,
    twitterUrl: String,
   role:{
    type:String,
    enum:['admin','user'],default:'user'
},  



   
   token:String,
  
   



},
{ timestamps: true, autoCreate: true, autoIndex: true }
);
const UserModel = mongoose.model("User",UserSchema)
module.exports=UserModel