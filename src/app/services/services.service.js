const {ServiceCreateSchema} = require("./services.validator")
const ServiceModel= require("./services.model")
class Services { 
    

    createService=async (data)=>{
        try{
            
            let Service= new ServiceModel(data)
          return await Service.save()

        }catch(exception){
            throw exception
        }

    }

   
    listAllService = async()=>{
        try{
        
            let Services=await ServiceModel.find()
           

            return Services;

        }catch(exception){
            throw exception
        }
    }


   getServiceById= async(id)=>{
    try{
        
        let Service= await ServiceModel.findById(id)
        
        return Service;
        
        
        
    }catch(exception){
        next(exception)
    }
   }


updateServiceById= async(data,id)=>{
    try{
       let response = await ServiceModel.findByIdAndUpdate(id,{
        $set:data
       }) 
       return response

    }catch(exception){
        throw exception
    }
}


deleteService= async(id)=>{
    try{
        let response = await ServiceModel.findByIdAndDelete(id)
        return response;

     


    }catch(exception){
        throw exception
    }

}





}
const serviceSvc= new Services()
module.exports= serviceSvc