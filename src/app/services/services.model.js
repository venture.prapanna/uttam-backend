const mongoose = require("mongoose");

const serviceSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
      minlength: 2,
    },

    description: {
      type: String,
      required: true,
    },

    photo: {
      type: String,
      required:true
    
    },

  

    createdBy: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      
      default: null,
    },
  },
  { timestamps: true, autoCreate: true, autoIndex: true }
);

const ServiceModel = mongoose.model("Service", serviceSchema);

module.exports = ServiceModel;
