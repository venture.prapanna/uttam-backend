const express = require("express")
const router = express.Router()
const serviceCtrl = require("./services.controller");
const CheckLogin = require("../../middleware/auth.middleware");
const CheckPermission = require("../../middleware/rbac.middleware");
const uploader = require("../../middleware/uploader.middleware");
const { ValidateRequest } = require("../../middleware/validator.middleware");
const { ServiceCreateSchema, ServiceUpdateSchema } = require("./services.validator");
const dirSet = (req, res, next) => {
    req.uploadDir = "./public/uploads/services/"
    next()
}
router.post("/",
CheckLogin,dirSet,CheckPermission('admin'), uploader.single('photo'),
 ValidateRequest(ServiceCreateSchema),serviceCtrl.createService);
router.get("/",
CheckLogin,CheckPermission('admin'),
serviceCtrl.getAllServices)
router.get("/:id",CheckLogin,CheckPermission('admin'),serviceCtrl.getServiceById);
router.put("/:id",CheckLogin,dirSet,CheckPermission('admin'),    uploader.single('photo'), ValidateRequest(ServiceUpdateSchema),serviceCtrl.updateService);
router.delete("/:id",CheckLogin,CheckPermission('admin'),serviceCtrl.deleteService);


module.exports= router;