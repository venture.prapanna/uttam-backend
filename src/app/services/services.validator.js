const {z} = require("zod")
const ServiceCreateSchema = z.object({
    title:z.string().min(1).max(100),
    description:z.string().min(1).max(500),
    photo:z.string()
    

})

const ServiceUpdateSchema = z.object({
    title:z.string().min(1).max(100),
    description:z.string().min(1).max(500),
    photo:z.string()
    

})
module.exports = {ServiceCreateSchema,ServiceUpdateSchema}


