const {ServiceUpdateSchema}=require("./services.validator.js")
const serviceSvc = require("./services.service.js");

class ServiceController {
  createService = async (req, res, next) => {
    try {
      let data = req.body;
      console.log(data)
      const imageName=req.file?req.file.filename:null;
      data.photo=imageName;
      let createdService = await serviceSvc.createService(data);
      res.json({
        result: createdService,
        message: "Service created Successfully",
        meta: null,
      });
    } catch (exception) {
      next(exception);
    }
  };

  getServiceById = async (req, res, next) => {
    try {
      let id = req.params.id;
      let serviceDetail = await serviceSvc.getServiceById(id);
      res.json({
        result: serviceDetail,
        message: "Service Detail Successfully fetched",
      });
    } catch (exception) {
      next(exception);
    }
  };

  getAllServices = async (req, res, next) => {
    try {
      const services = await serviceSvc.listAllService();
      res.json({
        result: services,
        message: "Services fetched successfully",
      });
    } catch (exception) {
      throw exception;
    }
  };

  updateService = async (req, res, next) => {
    try {
        const validatedData = ServiceUpdateSchema.parse(req.body)
      let serviceDetail = await serviceSvc.getServiceById(req.params.id);
      if (!serviceDetail) {
        next({ status: 404, message: "Service doesnot exist" });
      }
     
      let updatedService = await serviceSvc.updateServiceById(
        validatedData,
        serviceDetail._id
      );
      res.json({
        result:updatedService,
       
        message: "Services Updated Successfully",
      });
    } catch (exception) {
      throw exception;
    }
  };

  deleteService = async (req, res, next) => {
    try {
      let deletedData = await serviceSvc.deleteService(req.params.id);

      res.json({
        
        message: "Service deleted Successfully",
      });
    } catch (exception) {
      throw exception;
    }
  };
}

const serviceCtrl = new ServiceController();
module.exports = serviceCtrl;
